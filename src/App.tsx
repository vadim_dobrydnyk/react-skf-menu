import React from 'react';
import NodeElement from './components/NodeElement';
import './css/App.css';

export interface Node {
  name: string
  nodes?: Node[]
  children?: Node[]
}

const nodes: Node[] = [
  {
    name: 'Some node 1',
    nodes: [
      {
        name: 'Some node 1.1',
        children: [
          { name: 'Some node 1.1.1' },
        ]
      },
      { name: 'Some node 1.2' },
      {
        name: 'Some node 1.3',
        children: [
          { name: 'Some node 1.3.1' },
        ]
      },
      { name: 'Some node 1.4' }
    ]
  },
  {
    name: 'Some node 2'
  }
]


function App() {
  return (
    <div className='container'>
      {nodes.length && nodes.map((node, i) => <NodeElement key={i} node={node} />)}
    </div>
  );
}

export default App;
