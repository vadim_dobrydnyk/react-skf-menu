import { Node } from '../App'
import React, {useMemo, useState} from 'react';
import NodeChildren from './NodeChildren';

interface Props {
  node: Node
}

function NodeElement({node}: Props) {
  const [isOpen, setOpen] = useState(false)
  const haveChildNodes = useMemo(
    () => node.children?.length ||  node.nodes?.length,
    [node.children, node.nodes]
  )

  const handleClick = () => {
    console.log(node.name, node)

    if (haveChildNodes) {
      setOpen((isOpen) => !isOpen)
    }
  }

  return <div className={`node ${isOpen ? 'node--open' :''}`}>
    <div
      className='node__title'
      onClick={handleClick}
    >
      {haveChildNodes && <i className='node__icon' />}
      <p className='node__name'>{node.name}</p>
    </div>

    {node.children?.length && <NodeChildren children={node.children} />}

    {node.nodes?.length && <NodeChildren children={node.nodes} />}
  </div>
}

export default NodeElement
