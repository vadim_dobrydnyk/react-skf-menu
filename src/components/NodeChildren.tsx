import { Node } from '../App'
import NodeElement from './NodeElement';

interface Props {
  children: Node[]
}

function NodeChildren({children}: Props) {
  return <div className='node__children'>
    {children.map((node, i) => <NodeElement key={i} node={node} />)}
  </div>
}

export default NodeChildren
